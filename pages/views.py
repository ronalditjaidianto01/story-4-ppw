from django.views.generic import TemplateView
from django.shortcuts import redirect, render
from django.views.decorators.http import require_POST

from .forms import JadwalForm

from .models import Jadwal


def index(request):
    list_jadwal = Jadwal.objects.all()
    form = JadwalForm()
    context = {'form':form, 'list_jadwal':list_jadwal}
    return render(request, 'jadwal.html', context)

@require_POST
def add(request):
    form = JadwalForm(request.POST)
    if form.is_valid():
        form.save()
    return redirect('jadwal')

def delete(request, jadwal_id):
    jadwal = Jadwal.objects.get(pk=jadwal_id)
    jadwal.delete()

    return redirect('jadwal')

class ViewBerandaPage(TemplateView):
    template_name = 'beranda.html'

class ViewPengalamanPage(TemplateView):
    template_name = 'pengalaman.html'

class ViewProfilPage(TemplateView):
    template_name = 'profil.html'

class ViewKontakPage(TemplateView):
    template_name = 'kontak.html'


