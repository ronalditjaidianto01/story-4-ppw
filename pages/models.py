from django.db import models

# Create your models here.
class Jadwal(models.Model):
    hari = models.CharField(max_length=6)
    tanggal = models.DateField()
    jam = models.TimeField()
    namaKegiatan = models.CharField(verbose_name='Nama Kegiatan', max_length=15)
    tempat = models.CharField(max_length=10)
    kategori = models.CharField(max_length=10)