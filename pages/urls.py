from django.urls import path
from . import views
from .views import ViewBerandaPage, ViewProfilPage, ViewPengalamanPage, ViewKontakPage

urlpatterns = [
    path('', ViewBerandaPage.as_view(), name='beranda'),
    path('profil/', ViewProfilPage.as_view(), name='profil'),
    path('pengalaman/', ViewPengalamanPage.as_view(), name='pengalaman'),
    path('kontak/', ViewKontakPage.as_view(), name='kontak'),
    path('jadwal/', views.index, name='jadwal'),
    path('jadwal/add/', views.add, name='add'),
    path('jadwal/delete/<jadwal_id>', views.delete, name='delete')
]