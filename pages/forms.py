from django import forms
from django.forms import ModelForm
from .models import Jadwal
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

class JadwalForm(ModelForm):
    # def clean_tanggal(self):
    #     data = self.cleaned_data['tanggal']
        
    #     # Cek jika tanggal sudah berlalu
    #     if data < datetime.date.today():
    #         raise ValidationError(_('Tanggal tidak valid - tanggal sudah berlalu'))
    #     return data

    # def clean_hari(self):
    #     data = self.cleaned_data['hari']

    #     # Cek jika hari masih berisi instruksi default
    #     if data == 'Pilih Hari':
    #         raise ValidationError(_('Hari valid - pilih hari yang sesuai'))
    #     return data

    class Meta:
        model = Jadwal
        fields = ['hari', 'tanggal', 'jam', 'namaKegiatan', 'tempat', 'kategori']
        PILIHAN_HARI = [('def', 'Pilih Hari'),('Senin','Senin'), ('Selasa','Selasa'), ('Rabu','Rabu'), ('Kamis','Kamis'), ('Jumat','Jumat'), ('Sabtu','Sabtu'), ('Minggu','Minggu')]
        widgets = { 
            'hari': forms.Select(choices=PILIHAN_HARI),
            'tanggal': forms.DateInput(attrs={'placeholder':'mm/dd/yyyy', 'type':'date'}),
            'jam': forms.TimeInput(attrs={'placeholder':'hh:mm'}),
        }